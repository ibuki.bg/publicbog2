<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String result = (String)request.getAttribute("result");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPM-投稿結果</title>
<link rel="stylesheet" href="css/index.css" type="text/css">
</head>
<body>
<!-- header start -->
	<header>
		<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
		<nav>
<%if(session.getAttribute("id") == null){%>
	<a href="/WebKensyu/Login.jsp">ログイン</a>
	<a href="/WebKensyu/CreateUser.jsp">新規会員登録</a>
	<%-- TODO 記事一覧のリンク --%>
	<a href="/WebKensyu/ConArticleList">記事一覧</a>
<% }else{%>
<a href="/WebKensyu/Logout.jsp">ログアウト</a>
<%-- TODO 記事一覧のリンク --%>
<a href="/WebKensyu/ConArticleList">記事一覧</a>
<%-- TODO マイページのリンク --%>
<a href="/WebKensyu/MyPage">マイページ</a>
<%}%>
</nav>
	</header>

	<!-- header end -->

	<div id="main">
		<p><%= result %></p>
	</div>

	<!-- footer start -->
	<footer>
      <p>(c)copy right</p>
	</footer>
	<!-- footer end -->
</body>
</html>