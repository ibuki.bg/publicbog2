<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPM-ログイン</title>
<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
<nav>
</nav>
<style type="text/css">

footer{
    width: 100%;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 30px 0;
 	position: absolute;/*←絶対位置*/
    bottom: 0; /*下に固定*/
}
header{
	text-align:center;
}

</style>
</head>
<body>
<h2>ログイン画面</h2>
<p>IDとパスワードを入力し、ログインボタンを押してください</p>

<form action= "/WebKensyu/Login" method="POST">

<%if(session.getAttribute("err") != null){%>
	<p><font color="red"><%= session.getAttribute("err")%> </font></p>
	<%session.setAttribute("err", null); %>
<% }%>

	<table>
	<tr>
		<td>ID:</td>
		<td><input type="text" name="id" size="40" ></td>
	</tr>
	<tr>
		<td>PassWord:</td>
		<td><input type="text" name="pass" size="40" ></td>
	</tr>
	</table>
	<input type="submit" value="ログイン">
</form>
</body>
<footer>
      <p>(c)copy right</p>
</footer>
</html>