<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "beans.Article"%>
<%@ page import = "java.util.List" %>
<%@ page import = "beans.User"%>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>

<%
	Article article = (Article)request.getAttribute("article");
	User user = (User)request.getAttribute("user");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    Date dateFormat = sdf.parse(article.getDate());
    sdf.applyPattern("yyyy年MM月dd日");

    String date = String.valueOf(sdf.format(dateFormat));
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SMP-記事詳細</title>
<link rel="stylesheet" href="css/articleDetail.css" type="text/css">
<link rel="stylesheet" href="css/index.css" type="text/css">
</head>
<body>
	<!-- header start -->
	<header>
		<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
		<nav>
		<%if(session.getAttribute("id") == null){%>
			<a href="/WebKensyu/Login.jsp">ログイン</a>
			<a href="/WebKensyu/CreateUser.jsp">新規会員登録</a>
			<%-- TODO 記事一覧のリンク --%>
			<a href="/WebKensyu/ConArticleList">記事一覧</a>
		<% }else{%>
		<a href="/WebKensyu/Logout.jsp">ログアウト</a>
		<%-- TODO 記事一覧のリンク --%>
		<a href="/WebKensyu/ConArticleList">記事一覧</a>
		<%-- TODO マイページのリンク --%>
		<a href="/WebKensyu/MyPage">マイページ</a>
		<%}%>
		</nav>
	</header>

	<!-- header end -->

	<!-- main start -->
	<div id="main">

	<h1 class="title"><%= article.getTitle() %></h1>
	<p class="contents"><%= article.getContents() %></p>
	<div class="line"></div>
	<p class="name"><%= user.getMailAdress() %></p>
	<p class="date"><%= date %></p>

	<div class="clear"></div>

	<button class="back" onclick="history.back()">戻る</button>

	</div>
	<!-- main end -->

	<!-- footer start -->
	<footer>
      <p>(c)copy right</p>
	</footer>
	<!-- footer end -->
</body>
</html>