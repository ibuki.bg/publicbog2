<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "beans.Article"%>
<%@ page import = "beans.User"%>
<%@ page import = "java.util.List" %>
<%@ page import = "model.Dao"%>


<% 	List<Article> articlelist = (List<Article>)request.getAttribute("article");
	Dao dao = new Dao();
	User user = new User();
%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<title>SMP-記事一覧</title>
<header id="sample">
<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
<nav>
<%if(session.getAttribute("id") == null){%>
	<a href="/WebKensyu/Login.jsp">ログイン</a>
	<a href="/WebKensyu/CreateUser.jsp">新規会員登録</a>
	<%-- TODO 記事一覧のリンク --%>
	<a href="/WebKensyu/ConArticleList">記事一覧</a>
<% }else{%>
<a href="/WebKensyu/Logout.jsp">ログアウト</a>
<%-- TODO 記事一覧のリンク --%>
<a href="/WebKensyu/ConArticleList">記事一覧</a>
<%-- TODO マイページのリンク --%>
<a href="/WebKensyu/MyPage">マイページ</a>
<%}%>
</nav>
<style type="text/css">
footer{
    width: 100%;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 30px 0;
 	position: absolute;/*←絶対位置*/
    bottom: 0; /*下に固定*/
}
</style>
</header>
<body>
	<h1>記事一覧</h1>
	<% for (Article article : articlelist) {%>
	<% String url ="ConArticleDetail?id=" + article.getArticleId();
	   user = dao.getUser(article.getUserId());%>
     <a href = <%= url %>>タイトル:<%= article.getTitle()%>
     					  投稿者:<%= user.getMailAdress() %></a><br>
    <% } %>
</body>
<footer>
      <p>(c)copy right</p>
</footer>
</html>