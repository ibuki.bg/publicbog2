<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPM-ユーザー登録</title>
<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
<nav>
</nav>
<style type="text/css">

footer{
    width: 100%;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 30px 0;
 	position: absolute;/*←絶対位置*/
    bottom: 0; /*下に固定*/
}
header{
	text-align:center;
}

</style>
</head>
<body>
<h2>ユーザー新規作成</h2>
<p>IDとPassWordを入力し、登録ボタンを押してください</p>
<form action= "/WebKensyu/CreateUser" method="POST">
	<%if(session.getAttribute("err") != null){%>
		<p><font color="red"><%= session.getAttribute("err")%> </font></p>
		<%session.setAttribute("err", null); %>
	<% }%>
	<table>
		<tr>
			<td>ID:</td>
			<td><input type="id" name="id" size="40"  required></td>
		</tr>
		<tr>
			<td>PassWord:</td>
			<td><input type="pass" name="pass" size="40"  required></td>
		</tr>
	</table>
	<br>
	<input type="submit" value="登録">
</form>
</body>
<footer>
      <p>(c)copy right</p>
</footer>
</html>
