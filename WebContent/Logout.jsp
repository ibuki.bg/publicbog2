<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPM-ログアウト</title>
<%-- TODO トップのリンク --%>
<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
<nav>
	<a href="/WebKensyu/Login.jsp">ログイン</a>
	<a href="/WebKensyu/CreateUser.jsp">新規会員登録</a>
	<%-- TODO 記事一覧のリンク --%>
	<a href="/WebKensyu/ConArticleList">記事一覧</a>
</nav>
<style type="text/css">

footer{
    width: 100%;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 30px 0;
 	position: absolute;/*←絶対位置*/
    bottom: 0; /*下に固定*/
}
header{
	text-align:center;
}

</style>
</head>
<body>
ログアウトしました。
<%session.setAttribute("id", null); %>
</body>
<footer>
      <p>(c)copy right</p>
</footer>
</html>