<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "java.util.List"%>
<%@ page import = "beans.Article"%>
<%@ page import = "beans.User"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPM-マイページ</title>

<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
<nav>
<%if(session.getAttribute("id") == null){%>
	<a href="/WebKensyu/Login.jsp">ログイン</a>
	<a href="/WebKensyu/CreateUser.jsp">新規会員登録</a>
	<%-- TODO 記事一覧のリンク --%>
	<a href="/WebKensyu/ConArticleList">記事一覧</a>
<% }else{%>
<a href="/WebKensyu/Logout.jsp">ログアウト</a>
<%-- TODO 記事一覧のリンク --%>
<a href="/WebKensyu/ConArticleList">記事一覧</a>
<%-- TODO マイページのリンク --%>
<a href="/WebKensyu/MyPage">マイページ</a>
<%}%>
</nav>
<style type="text/css">

footer{
    width: 100%;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 30px 0;
 	position: absolute;/*←絶対位置*/
    bottom: 0; /*下に固定*/
}
header{
	text-align:center;
}

</style>
</head>
<body>
<%
	List<Article> ac = (List<Article>)request.getAttribute("ac");
	User user = (User)request.getAttribute("user");
%>
<br><br>
  <table>
    <caption style="width: 207px">記事一覧(<%= user.getMailAdress() %>さん)</caption>
    <tr>
      <td>タイトル</td>
    </tr>
<%
 int i = 1;
 for (Article tmp : ac) {
    out.print("<tr>");
    out.print("<td><a href=\"ConArticleDetail?id=" + tmp.getArticleId() + "\">" + tmp.getTitle() + "</td>");
    out.print("</tr>");
    i++;
 } %>
  </table>
  <button onclick="location.href='/WebKensyu/ConEditArticle'">新規投稿</button>
</body>
<footer>
      <p>(c)copy right</p>
</footer>
</html>