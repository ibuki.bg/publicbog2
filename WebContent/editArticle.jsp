<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "beans.Article"%>

<%
	Article article = (Article)request.getAttribute("article");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SPM-記事作成</title>
<link rel="stylesheet" href="css/index.css" type="text/css">
</head>


<h1><a href="/WebKensyu/Top"><img src="image/logo.jpg" alt="ロゴ"  width=800 height=100></a></h1>
<header>
<nav>
<%if(session.getAttribute("id") == null){%>
	<a href="/WebKensyu/Login.jsp">ログイン</a>
	<a href="/WebKensyu/CreateUser.jsp">新規会員登録</a>
	<%-- TODO 記事一覧のリンク --%>
	<a href="/WebKensyu/ConArticleList">記事一覧</a>
<% }else{%>
<a href="/WebKensyu/Logout.jsp">ログアウト</a>
<%-- TODO 記事一覧のリンク --%>
<a href="/WebKensyu/ConArticleList">記事一覧</a>
<%-- TODO マイページのリンク --%>
<a href="/WebKensyu/MyPage">マイページ</a>
<%}%>
</nav>
</header>

	<!-- header end -->

	<div id="main">
	<form action="/WebKensyu/registerArticle" method="POST">
	<% if (article != null) { %>
		<h1>タイトル&ensp;<input type="text" name="title" value="<%= article.getTitle() %>" required/></h1>
		<h1>本文</h1>
		<p><textarea rows=10 cols=100 name="content" ><%= article.getContents() %></textarea></p>
		<input type="hidden" value="<%= article.getArticleId() %>" name="articleId">
	<% } else { %>
		<h1>タイトル&ensp;<input type="text" name="title" required/></h1>
		<h1>本文</h1>
		<p><textarea rows=10 cols=100 name="content"></textarea></p>
		<input type="hidden" value="" name="articleId">
	<% } %>
	<input type="submit" value="送信">
	</form>
	</div>

	<!-- footer start -->
	<footer>
      <p>(c)copy right</p>
	</footer>
	<!-- footer end -->
</body>
</html>