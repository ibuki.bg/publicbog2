package beans;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private int userID;
	private String mailAdress, passWord, deleteTime;

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public void setMailAdress(String mail) {
		this.mailAdress = mail;
	}

	public void setPassWord(String pass) {
		this.passWord = pass;
	}

	public void setDeleteTime(String time) {
		this.deleteTime = time;
	}

	public String getPassWord() {
		return passWord;
	}

	public int getUserID() {
		return userID;
	}

	public String getMailAdress() {
		return mailAdress;
	}

	public String deleteTime() {
		return deleteTime;
	}

}
