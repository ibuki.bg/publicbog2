package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Dao;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
	}

	static final String dbName = "blogdb";
	static final String user = "root", pass = "admin";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		Dao dao = new Dao();
		int ckResult = dao.checkUser(request.getParameter("id"), request.getParameter("pass"));
		HttpSession session = request.getSession();
		if (ckResult <= 0) {
			session.setAttribute("err", "IDかパスワードが間違っています");
			response.sendRedirect("/Login.jsp");
		} else {
			session.setAttribute("id", String.valueOf(ckResult));
			response.sendRedirect("/WebKensyu/Top");
		}

	}

}
