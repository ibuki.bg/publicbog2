package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Dao;

/**
 * Servlet implementation class CreateUser
 */
@WebServlet("/CreateUser")
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static final String dbName = "blogdb";
	static final String user = "root", pass = "admin";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateUser() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");

		Dao dao = new Dao();
		int ckResult = dao.checkUser(request.getParameter("id"), request.getParameter("pass"));
		HttpSession session = request.getSession();
		if (!(ckResult <= 0)) {
			session.setAttribute("err", "そのメールアドレスは既に登録されています");
			getServletContext().getRequestDispatcher("/CreateUser.jsp")
					.forward(request, response);
		} else {
			dao.createUser(request.getParameter("id"), request.getParameter("pass"));

			getServletContext().getRequestDispatcher("/CreateUser2.jsp")
					.forward(request, response);
		}
	}

}
