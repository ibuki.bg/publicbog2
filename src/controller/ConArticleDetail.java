package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Article;
import beans.User;
import model.Dao;

/**
 * Servlet implementation class ConArticleDetail
 */
@WebServlet("/ConArticleDetail")
public class ConArticleDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConArticleDetail() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String articleId = request.getParameter("id");
		String viewer = (String) request.getSession().getAttribute("id");

		if (viewer == null) {
			viewer = "0";
		}

		Dao dao = new Dao();

		if (articleId == null) {
			articleId = "1";
		}
		dao.addPv(articleId, viewer);

		Article ac = dao.getArticle(Integer.parseInt(articleId));

		if (ac == null) {
			response.sendRedirect("/WebKensyu/ConArticleList.jsp");
			return;
		}

		User user = dao.getUser(ac.getUserId());

		request.setAttribute("article", ac);
		request.setAttribute("user", user);
		request.getRequestDispatcher("./articleDetail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
