package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Dao;

/**
 * Servlet implementation class RegisterArticle
 */
@WebServlet("/registerArticle")
public class RegisterArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterArticle() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String articleId = request.getParameter("articleId");
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("id");

		String result = "";

		if (title == "") {
			response.sendRedirect("/WebKensyu/ConEditArticle");
			return;
		}

		Dao dao = new Dao();

		if (dao.createArticle(Integer.parseInt(userId), title, content, "") == 0) {
			result = "登録出来ませんでした";
		} else {
			result = "登録完了";
		}

		request.setAttribute("result", result);

		request.getRequestDispatcher("/registerArticle.jsp").forward(request, response);
	}

}
